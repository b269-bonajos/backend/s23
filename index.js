// console.log("Hello!")

let games = {
	name: "Ash Ketchum",
	age: 10

	

}
console.log(games)

let stats = {
	name: "Pikachu",
	level: 39,
	health: 98,
	attack: 17,
	friends: {haenn: ["May", "Max"], kanto: ["Brock", "Misty"]},
}
console.log(stats)

function pokemon(name, level, health, attack) {

	this.name = name;
	this.level = level
	this.health = health
	this.attack = attack

	this.tackle = function(character){
	console.log(this.name + 'tackle ' + character.name)
	console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	};	
	this.faint = function(){
		console.log(this.name + 'fainted.');
	}
};
pokemon()


// New Characters
let character = new pokemon("Geodude", 28, 85, 12)
console.log(character)

let newCharacter = new pokemon("Mewtoo", 80, 28, 42)
console.log(newCharacter)
console.log("Geodude tackle Pikachu")
console.log("Pikachu's health is now reduced to 65")

let pikachu = new pokemon("Pikachu", 55, 66, 77,)
console.log(pikachu)
console.log("Pikachu tackle Geodude ")
console.log("Geodude's health is now reduced to -65")
console.log("Geodude fainted")

console.log(character)